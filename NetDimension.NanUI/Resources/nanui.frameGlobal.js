﻿(function () {
	window["raiseCustomEvent"] = function (eventName, customDetail) {
		window.dispatchEvent(new CustomEvent(eventName, { detail: customDetail }));
	};

	const ATTR_NAME = "n-ui-command";
	
	document.addEventListener("DOMContentLoaded", () => {
		
		document.body.addEventListener("click", (e) => {

			console.log("DOM CLICK");

			var targetEl = e.srcElement;

			while (targetEl && !targetEl.hasAttribute(ATTR_NAME)) {
				targetEl = targetEl.parentElement;
			}

			if (targetEl) {
				var cmd = targetEl.getAttribute(ATTR_NAME);

				if (cmd && NanUI) {
					NanUI.hostWindow[cmd].apply(NanUI, [e]);
				}
			}
		});

		console.log("dom ready");

		raiseCustomEvent("hostactivestate", {state: 1, stateName:"activated"});

		document.removeEventListener("DOMContentLoaded", arguments.callee, false);
	});
})();

